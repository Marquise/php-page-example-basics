<?php
include 'blog-functions.php';
$blog_template = [
	'header_elements' => [
		'logo_src' => "https://google.com/logo.png",
		'logo_link' => "/",
		'logo_alt' => 'alt tekst',
	],
	'navigation' => [
		'item 1 Label' => 'item1Link',
		'item 2 Label' => 'item2Link',
	],
	'blogNav' => [
		'prevLabel' => 'Prošli',
		'nextLabel' => 'Sledeći',
	],
	'footer' => [
		'footerCopy' => "Copyright &copy; 2021"
	]
];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<style>
		.h-richtext {
			p {
				font-size: 15px;
			}
		}
	</style>
</head>
<body>
	<header>
		<?php if($blog_template['header_elements']['logo_src']): ?>
			<div class="logo">
				<a href="<?php echo $blog_template['header_elements']['logo_link']; ?>">
					<img 
						src="<?php echo $blog_template['header_elements']['logo_src']; ?>" 
						alt="<?php echo $blog_template['header_elements']['logo_alt']; ?>"
					/>
				</a>
			</div>
		<?php endif; ?>
		<?php if (sizeof($blog_template['navigation']) > 0): ?>
			<nav>
				<ul>
					<?php foreach ($blog_template['navigation'] as $nav_label => $nav_link): ?>
						<li><a href="<?php echo $nav_link; ?>"><?php echo $nav_label; ?></a></li>
					<?php endforeach; ?>
				</ul>
			</nav>
		<?php endif; ?>
	</header>

	<?php 
		$article_content = getContent(1);
		include 'article2.php'; 
	?>
	

	<footer>
		<?php echo $blog_template['footer']['footer_copy']; ?>
	</footer>
</body>
</html>